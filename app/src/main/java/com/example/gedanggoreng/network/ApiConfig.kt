package com.example.gedanggoreng.network

import android.content.Context
import android.os.Build
import com.example.gedanggoreng.BuildConfig
import com.readystatesoftware.chuck.ChuckInterceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

object ApiConfig {
    fun services(context: Context): ApiRepository {
        val logger = OkHttpClient.Builder()
        val httpLoggingInterceptor = HttpLoggingInterceptor()
        httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.HEADERS
        httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

        logger.addInterceptor(httpLoggingInterceptor)
        logger.addInterceptor(ChuckInterceptor(context))


        val retrofit = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(logger.build())
            .baseUrl(BuildConfig.BASE_URL)
            .build()
        return retrofit.create(ApiRepository::class.java)
    }
}