package com.example.gedanggoreng.network

import com.example.gedanggoreng.model.UsersResponse
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.GET

interface ApiRepository {
    @GET("users")
    fun getUsers(): Observable<Response<UsersResponse>>
}