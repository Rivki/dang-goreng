package com.example.gedanggoreng.util

object AppConstants{
    class ApiStatusCode{
        companion object{
            val FORBIDDEN = 403
            val INTERNAL_SERVER_ERROR = 500
            val NOT_FOUND = 404
            val UNAUTHORIZED = 401
            val EXPIRED = 408
        }
    }
}