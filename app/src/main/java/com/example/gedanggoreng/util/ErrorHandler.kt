package com.example.gedanggoreng.util

import android.util.Log
import com.example.gedanggoreng.network.ErrorResponse
import retrofit2.HttpException

class ErrorHandler {
    fun handleApiError(error: HttpException) {
        if (error.code() == AppConstants.ApiStatusCode.EXPIRED ||
            error.code() == AppConstants.ApiStatusCode.FORBIDDEN ||
            error.code() == AppConstants.ApiStatusCode.UNAUTHORIZED ||
            error.code() == AppConstants.ApiStatusCode.NOT_FOUND
        ) {

        }

        val errorMessage = ErrorResponse.fromRaw(error.response()?.errorBody()?.string()!!).message

        if (error != null) {
            Log.e("TAG ERROR NETWORK", error.toString())
        } else {
            Log.e("TAG", error.toString())
        }
        Log.e("ERROR", error.localizedMessage)
    }
}