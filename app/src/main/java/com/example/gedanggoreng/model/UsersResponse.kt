package com.example.gedanggoreng.model


import com.google.gson.annotations.SerializedName

data class UsersResponse(
    @SerializedName("data")
    val data: List<Data>
) {
    data class Data(
        @SerializedName("address")
        val address: String,
        @SerializedName("createdAt")
        val createdAt: String,
        @SerializedName("first_name")
        val firstName: String,
        @SerializedName("id")
        val id: Int,
        @SerializedName("last_name")
        val lastName: String,
        @SerializedName("updateAt")
        val updateAt: String
    )
}