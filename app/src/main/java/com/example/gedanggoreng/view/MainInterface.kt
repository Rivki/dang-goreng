package com.example.gedanggoreng.view

import com.example.gedanggoreng.model.UsersResponse

interface MainInterface {
    fun showData(data: UsersResponse)
}