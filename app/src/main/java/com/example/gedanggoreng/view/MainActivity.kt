package com.example.gedanggoreng.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.example.gedanggoreng.R
import com.example.gedanggoreng.model.UsersResponse
import com.example.gedanggoreng.presenter.UsersPresenter
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), MainInterface {

    private lateinit var presenter: UsersPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        presenter = UsersPresenter(this, this@MainActivity)
        presenter.getAll()
    }

    override fun showData(data: UsersResponse) {
        val user = data.data.size
        for (i in 0 until user) {
            text_view.text = data.data[i].firstName
            Log.d("DATANYA", data.data[i].firstName)
        }
    }
}
