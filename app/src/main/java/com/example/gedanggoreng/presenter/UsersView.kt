package com.example.gedanggoreng.presenter

import com.example.gedanggoreng.view.MainInterface

interface UsersView {
    fun getAll()
}