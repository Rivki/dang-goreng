package com.example.gedanggoreng.presenter

import android.content.Context
import android.util.Log
import com.example.gedanggoreng.network.ApiConfig
import com.example.gedanggoreng.util.ErrorHandler
import com.example.gedanggoreng.util.SchedulerProvider
import com.example.gedanggoreng.view.MainInterface
import io.reactivex.disposables.CompositeDisposable
import retrofit2.HttpException

class UsersPresenter(val main: MainInterface, val context: Context) : UsersView {
    override fun getAll() {
        val api = ApiConfig.services()
        val getData = api.getUsers()
        val compositeDisposable = CompositeDisposable()
        val schedulerProvider = SchedulerProvider()
        val errorHandler = ErrorHandler()
        compositeDisposable.add(
            getData
                .compose(schedulerProvider.ioToMainObservableScheduler())
                .subscribe({ response ->
                    if (response.isSuccessful) {
                        response.body()?.let { main.showData(it) }
                    }
                }, { e ->
                    when (e) {
                        is HttpException -> errorHandler.handleApiError(e)
                    }
                })
        )

    }
}